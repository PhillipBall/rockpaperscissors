﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random ran = new Random();
            int rand = ran.Next(4);
            label5.Visible = false;
            label9.Visible = false;
            label10.Visible = false;

            if (radioButton1.Checked == true)
            {
                if (rand == 1)
                {
                    label8.Text = "Tie!";
                    label10.Visible = true;
                }
                if (rand == 2)
                {
                    label8.Text = "Lost!";
                    MyGlobals.CScore = MyGlobals.CScore + 1;
                    label7.Text = MyGlobals.CScore.ToString();
                    progressBar1.Value = MyGlobals.CScore;
                    label9.Visible = true;
                    if (progressBar1.Value == 10)
                    {
                        MessageBox.Show("You Lost!");
                        button1.Enabled = false;
                    }
                }
                if (rand == 3)
                {
                    label8.Text = "Win!";
                    MyGlobals.Score = MyGlobals.Score + 1;
                    label6.Text = MyGlobals.Score.ToString();
                    progressBar2.Value = MyGlobals.Score;
                    label5.Visible = true;
                    if (progressBar2.Value == 10)
                    {
                        MessageBox.Show("You won!");
                        button1.Enabled = false;
                    }
                }

            }

            if (radioButton2.Checked == true)
            {
                if (rand == 1)
                {
                    label8.Text = "Win!";
                    MyGlobals.Score = MyGlobals.Score + 1;
                    label6.Text = MyGlobals.Score.ToString();
                    progressBar2.Value = MyGlobals.Score;
                    label5.Visible = true;
                    if (progressBar2.Value == 10)
                    {
                        MessageBox.Show("You won!");
                        button1.Enabled = false;
                    }
                }
                if (rand == 2)
                {
                    label8.Text = "Tie!";
                    label10.Visible = true;
                }
                if (rand == 3)
                {
                    label8.Text = "Lost!";
                    MyGlobals.CScore = MyGlobals.CScore + 1;
                    label7.Text = MyGlobals.CScore.ToString();
                    progressBar1.Value = MyGlobals.CScore;
                    label9.Visible = true;
                    if (progressBar1.Value == 10)
                    {
                        MessageBox.Show("You Lost!");
                        button1.Enabled = false;

                    }
                }

            }

            if (radioButton3.Checked == true)
            {
                if (rand == 1)
                {
                    label8.Text = "Lost!";
                    MyGlobals.CScore = MyGlobals.CScore + 1;
                    label7.Text = MyGlobals.CScore.ToString();
                    progressBar1.Value = MyGlobals.CScore;
                    label9.Visible = true;
                    if (progressBar1.Value == 10)
                    {
                        MessageBox.Show("You Lost!");
                        button1.Enabled = false;

                    }
                }
                if (rand == 2)
                {
                    label8.Text = "Win!";
                    MyGlobals.Score = MyGlobals.Score + 1;
                    label6.Text = MyGlobals.Score.ToString();
                    progressBar2.Value = MyGlobals.Score;
                    label5.Visible = true;
                    if (progressBar2.Value == 10)
                    {
                        MessageBox.Show("You won!");
                        button1.Enabled = false;
                    }
                }
                if (rand == 3)
                {
                    label8.Text = "Tie!";
                    label10.Visible = true;
                }

            }
        }

        private void progressBar2_Click(object sender, EventArgs e)
        {
           
        }
        public static class MyGlobals
        {
            public static int Score = 0;
            public static int CScore = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label6.Text = MyGlobals.Score.ToString();
            label7.Text = MyGlobals.CScore.ToString();
        }
    }
}
